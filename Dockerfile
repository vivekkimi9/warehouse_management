#
# Build application using Maven
#
FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
MAINTAINER Vivek SACHIDANANDA
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package -Dmaven.test.skip=true

#
# Load the JAR on to a lite OpenJDK JRE
#
# Base Alpine Linux based image with OpenJDK JRE only
FROM adoptopenjdk/openjdk11:alpine-jre
# copy application JAR (with libraries inside)
COPY --from=MAVEN_BUILD /build/target/*-0.0.1-SNAPSHOT.jar /warehouse.jar

# specify default command
ENTRYPOINT ["java","-jar","/warehouse.jar"]