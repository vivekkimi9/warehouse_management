package com.warehouse.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.core.DBManager;
import com.warehouse.data.IArticleRepository;
import com.warehouse.data.IProductArticleRepository;
import com.warehouse.data.IProductRepository;
import com.warehouse.data.entity.Product;
import com.warehouse.services.impl.ProductServiceImpl;


/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
public class ProductServiceImplTest {
	
    static {
        System.setProperty("warehouse,product.page.max-size", "100");
    	System.setProperty("warehouse.article.upload.batch-size", "15");
    }

    @TestConfiguration
    static class ProductServiceImplTestContextConfiguration {
 
        @Bean
        public IProductService productService() {
            return new ProductServiceImpl();
        }
    }

    @Autowired
    private IProductService productService;
    
    @MockBean
    private IArticleRepository articleRepository;

    @MockBean
    private IProductRepository productRepository;
    
    @MockBean
    private IProductArticleRepository productArticleRepository;
    
    @MockBean
    private DBManager dbManager;
    
    
    @SuppressWarnings("unchecked")
	@Before
    public void setUp() {
    	List<Product> returnProducts = new ArrayList<>();
    	Product prod = new Product();
    	prod.setName("Product 1");
    	prod.setStock(5);
    	returnProducts.add(prod);
    	
    	prod = new Product();
    	prod.setName("Product 1");
    	prod.setStock(5);
    	returnProducts.add(prod);
    	
    	prod = new Product();
    	prod.setName("Product 1");
    	prod.setStock(5);
    	returnProducts.add(prod);
    	
    	Pageable paging = PageRequest.of(0, 15);
    	Page<Product> products = Mockito.mock(Page.class);
    	Mockito.when(productRepository.findAll(paging)).thenReturn(products);
    }

    // write test cases here
    @Test
    public void whenPageable_thenReturnProducts() {
        Page<Product> products = productService.getProducts(PageRequest.of(0, 15));
         assertThat(products).isNotNull();
     }
    
}
