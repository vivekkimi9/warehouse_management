package com.warehouse.data;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.data.entity.Product;

/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IProductRepository productRepository;

    @Test
    public void whenFindById_thenReturnProduct() {
        // given
        Product prod = new Product();
        prod.setName("Product 1");
        
       Product saved = entityManager.persist(prod);
        entityManager.flush();

        // when
        Optional<Product> found = productRepository.findById(saved.getId());

        // then
        assertThat(found.isEmpty()).isFalse();
        assertThat(found.get().getName()).isEqualTo(prod.getName());
    }
}