package com.warehouse.data;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.data.entity.Article;


/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ArticleRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IArticleRepository articleRepository;

    @Test
    public void whenFindByArticleId_thenReturnArticle() {
        // given
    	Article article1 = new Article();
    	article1.setArticleId(1);
    	article1.setName("Article 1");
    	article1.setStock(4);
    	
    	Article article2 = new Article();
    	article2.setArticleId(2);
    	article2.setName("Article 2");
    	article2.setStock(9);
        
       entityManager.persist(article1);
       entityManager.persist(article2);
       entityManager.flush();

        // when
       Optional<Article> found = articleRepository.findByArticleId(1);

        // then
        assertThat(found.isEmpty()).isFalse();
        assertThat(found.get().getName()).isEqualTo(article1.getName());
    }
    
    @Test
    public void whenFindByArticleIds_thenReturnArticles() {
        // given
    	Article article1 = new Article();
    	article1.setArticleId(1);
    	article1.setName("Article 1");
    	article1.setStock(4);
    	
    	Article article2 = new Article();
    	article2.setArticleId(2);
    	article2.setName("Article 2");
    	article2.setStock(9);
    	
    	Article article3 = new Article();
    	article3.setArticleId(3);
    	article3.setName("Article 3");
    	article3.setStock(9);
        
       entityManager.persist(article1);
       entityManager.persist(article2);
       entityManager.persist(article3);
       entityManager.flush();
       
       Set<Integer> articleIds = new HashSet<>();
       articleIds.add(article1.getArticleId());
       articleIds.add(article2.getArticleId());
       
        // when
       List<Article> found = articleRepository.findAllByArticleIdIn(articleIds);

        // then
        assertThat(found.isEmpty()).isFalse();
        assertThat(found.size()).isEqualTo(2);
        assertThat(articleRepository.count()).isEqualTo(3L);
    }
}
