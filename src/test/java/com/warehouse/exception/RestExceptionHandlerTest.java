package com.warehouse.exception;


import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.warehouse.core.exceptions.ErrorConstants;
import com.warehouse.core.exceptions.InternalServerException;
import com.warehouse.core.exceptions.OutOfStockException;
import com.warehouse.services.impl.ProductServiceImpl;


/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class RestExceptionHandlerTest {

    @Autowired
    WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @MockBean
    ProductServiceImpl productService;

    @PostConstruct
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.applicationContext).build();
    }

    @Test
    @DisplayName("Test to validate the internal server error response")
    public void testInternalServerError() throws Exception {
    	Mockito.doThrow(InternalServerException.class).when(productService).sellById(ArgumentMatchers.anyString());
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/product/123/sell")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andReturn();
        Assertions.assertTrue(result.getResponse().getContentAsString().contains(ErrorConstants.InternalError.DOCS));
    }

    @Test
    @DisplayName("Test to validate the bad request ")
    public void testBadRequest() throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/product?size=1000")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andReturn();
        Assertions.assertTrue(result.getResponse().getContentAsString().contains(ErrorConstants.GenericError.DOCS));
    }
    
    @Test
    @DisplayName("Test to validate the Out of stock error response")
    public void testNotFoundError() throws Exception {
    	Mockito.doThrow(OutOfStockException.class).when(productService).sellById(ArgumentMatchers.anyString());
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/product/123/sell")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotAcceptable())
                .andReturn();
        Assertions.assertTrue(result.getResponse().getContentAsString().contains(ErrorConstants.OutOfStock.DOCS));
    }

}
