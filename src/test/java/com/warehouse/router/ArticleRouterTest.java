package com.warehouse.router;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.data.entity.Article;

/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ArticleRouterTest {

	@Autowired
	WebApplicationContext applicationContext;

	private MockMvc mockMvc;

	@PostConstruct
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.applicationContext).build();
	}

	@Test
	@DisplayName("Test upload of Article small batch")
	public void testArticleUploadSmallBatch_Http_200() throws Exception {
		MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/article/upload").file(articleJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		final ObjectMapper objectMapper = new ObjectMapper();
		Article[] articles = objectMapper.readValue(result.getResponse().getContentAsString(), Article[].class);
		Assertions.assertEquals(3, articles.length);
	}

	@Test
	@DisplayName("Test upload of Article big batch")
	public void testArticleUploadBigBatch_Http_200() throws Exception {
		MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25},{\"art_id\":3,\"name\":\"PLASMOS\",\"stock\":17},{\"art_id\":4,\"name\":\"COGNICODE\",\"stock\":15},{\"art_id\":5,\"name\":\"MAXEMIA\",\"stock\":23},{\"art_id\":6,\"name\":\"ECLIPSENT\",\"stock\":28},{\"art_id\":7,\"name\":\"CEDWARD\",\"stock\":29},{\"art_id\":8,\"name\":\"DEEPENDS\",\"stock\":16},{\"art_id\":9,\"name\":\"ELEMANTRA\",\"stock\":23},{\"art_id\":10,\"name\":\"ICOLOGY\",\"stock\":16},{\"art_id\":11,\"name\":\"KATAKANA\",\"stock\":21},{\"art_id\":12,\"name\":\"MUSIX\",\"stock\":29},{\"art_id\":13,\"name\":\"XLEEN\",\"stock\":21},{\"art_id\":14,\"name\":\"OVERPLEX\",\"stock\":29},{\"art_id\":15,\"name\":\"EMTRAC\",\"stock\":26},{\"art_id\":16,\"name\":\"BEDLAM\",\"stock\":27},{\"art_id\":17,\"name\":\"JIMBIES\",\"stock\":23},{\"art_id\":18,\"name\":\"AMRIL\",\"stock\":20},{\"art_id\":19,\"name\":\"WAZZU\",\"stock\":23},{\"art_id\":20,\"name\":\"CIPROMOX\",\"stock\":16},{\"art_id\":21,\"name\":\"COMBOT\",\"stock\":18},{\"art_id\":22,\"name\":\"JOVIOLD\",\"stock\":30},{\"art_id\":23,\"name\":\"QUINTITY\",\"stock\":21},{\"art_id\":24,\"name\":\"ERSUM\",\"stock\":25},{\"art_id\":25,\"name\":\"OPPORTECH\",\"stock\":19},{\"art_id\":26,\"name\":\"OZEAN\",\"stock\":30},{\"art_id\":27,\"name\":\"MARTGO\",\"stock\":24},{\"art_id\":28,\"name\":\"APPLICA\",\"stock\":15},{\"art_id\":29,\"name\":\"UTARIAN\",\"stock\":24},{\"art_id\":30,\"name\":\"TALKOLA\",\"stock\":30},{\"art_id\":31,\"name\":\"ECOLIGHT\",\"stock\":24},{\"art_id\":32,\"name\":\"CINESANCT\",\"stock\":24},{\"art_id\":33,\"name\":\"TRI@TRIBALOG\",\"stock\":21},{\"art_id\":34,\"name\":\"BIOSPAN\",\"stock\":22}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/article/upload").file(articleJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		final ObjectMapper objectMapper = new ObjectMapper();
		Article[] articles = objectMapper.readValue(result.getResponse().getContentAsString(), Article[].class);
		Assertions.assertEquals(35, articles.length);
	}

	@Test
	@DisplayName("Test upload of Article failed due to wrong json")
	public void testArticleUpload_Http_500() throws Exception {
		MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{[\"products\":{\"name\":\"VIAGRAND\",\"contain_articles\":[{\"art_id\":19,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAWEAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/article/upload").file(articleJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().is5xxServerError())
				.andExpect(MockMvcResultMatchers.content().json("{}")).andReturn();
		Assertions.assertNotNull(result);
	}

	@Test
	@DisplayName("Test get article")
	public void testArticleGet_Http_200() throws Exception {
		MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/article/upload").file(articleJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		final ObjectMapper objectMapper = new ObjectMapper();
		Article[] articles = objectMapper.readValue(result.getResponse().getContentAsString(), Article[].class);
		Assertions.assertEquals(3, articles.length);

		MvcResult getArticleResult = mockMvc
				.perform(MockMvcRequestBuilders.get(String.format("/article/1")).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		Article article = objectMapper.readValue(getArticleResult.getResponse().getContentAsString(), Article.class);
		Assertions.assertEquals(18, article.getStock());
		Assertions.assertEquals(1, article.getArticleId());
	}

	@Test
	@DisplayName("Test get unknown article")
	public void testUnknownArticleGet_Http_200() throws Exception {
		MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/article/upload").file(articleJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		final ObjectMapper objectMapper = new ObjectMapper();
		Article[] articles = objectMapper.readValue(result.getResponse().getContentAsString(), Article[].class);
		Assertions.assertEquals(3, articles.length);

		MvcResult getArticleResult = mockMvc
				.perform(MockMvcRequestBuilders.get(String.format("/article/7")).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertTrue(getArticleResult.getResponse().getContentAsString().isEmpty());
	}

}