package com.warehouse.router;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.data.entity.Article;
import com.warehouse.data.entity.Product;
import com.warehouse.dto.ArticleCountDTO;
import com.warehouse.dto.ProductDTO;

/**
 * @author viveksachidananda
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductRouterTest {

	@Autowired
	WebApplicationContext applicationContext;

	private MockMvc mockMvc;

	@PostConstruct
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.applicationContext).build();
	}
	
	@Test
	@DisplayName("Test post of Product")
	public void testProductPost_Http_200() throws Exception {
		final ObjectMapper objectMapper = new ObjectMapper();
		ProductDTO product = new ProductDTO();
		// build the product
		{
			List<ArticleCountDTO> articleArray = new ArrayList<>();
			ArticleCountDTO article1 = new ArticleCountDTO();
			article1.setAmount_of(5);
			article1.setArt_id(1);
			articleArray.add(article1);
			ArticleCountDTO article2 = new ArticleCountDTO();
			article2.setAmount_of(1);
			article2.setArt_id(2);
			product.setName("Chair");
			articleArray.add(article2);
			product.setContain_articles(articleArray);
		}
		
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/product")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(product))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);
		Product resultProduct = objectMapper.readValue(result.getResponse().getContentAsString(), Product.class);
		Assertions.assertNotNull(resultProduct);
		Assertions.assertEquals(product.getName(), resultProduct.getName());
		Assertions.assertFalse(resultProduct.getId().isEmpty());
	}

	@Test
	@DisplayName("Test upload of Product")
	public void testProductUpload_Http_200() throws Exception {
		MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"products\":[{\"name\":\"VIAGRAND\",\"contain_articles\":[{\"art_id\":19,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAWEAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/product/upload").file(productJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);
		final ObjectMapper objectMapper = new ObjectMapper();
		Product[] products = objectMapper.readValue(result.getResponse().getContentAsString(), Product[].class);

		Assertions.assertEquals(2, products.length);
	}

	@Test
	@DisplayName("Test get of Products")
	public void testGetProducts_Http_200() throws Exception {
		MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"products\":[{\"name\":\"VIAGRAND\",\"contain_articles\":[{\"art_id\":19,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAWEAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/product/upload").file(productJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		result = mockMvc.perform(MockMvcRequestBuilders.get("/product").accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		Assertions.assertNotNull(result);

		final ObjectMapper objectMapper = new ObjectMapper();
		@SuppressWarnings("unchecked")
		HashMap<String, Object> pageResponse = objectMapper.readValue(result.getResponse().getContentAsString(),
				HashMap.class);
		Assertions.assertEquals(2, pageResponse.get("numberOfElements"));
	}

	@Test
	@DisplayName("Test upload of Product failed due to wrong json")
	public void testProductUpload_Http_500() throws Exception {
		MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"products\":{\"name\":\"VIAGRAND\",\"contain_articles\":[{\"art_id\":19,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAWEAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
						.getBytes());
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.multipart("/product/upload").file(productJsonFile)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().is5xxServerError())
				.andExpect(MockMvcResultMatchers.content().json("{}")).andReturn();
		Assertions.assertNotNull(result);
	}

	@Test
	@DisplayName("Test sell of product with bad product id format")
	public void testUnknownProductSell_Http_500() throws Exception {
		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/123/sell"))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isInternalServerError()).andReturn();
		Assertions.assertNotNull(sellResult);
	}

	@Test
	@DisplayName("Test sell of unknown product with good product id format")
	public void testUnknownProductSell_Http_406() throws Exception {
		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/ff808081773ab72901773b05451f003a/sell"))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
		Assertions.assertNotNull(sellResult);
		Assertions.assertTrue(
				sellResult.getResponse().getContentAsString().contains("https://dummy.docs.warehouse.com/not-found"));
	}

	@Test
	@DisplayName("Test sell of Product without article")
	public void testSellProduct_Http_406() throws Exception {
		MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
				"{\"products\":[{\"name\":\"VIAGRAD\",\"contain_articles\":[{\"art_id\":19,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAEAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
						.getBytes());
		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/product/upload")
				.file(productJsonFile).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk());

		MvcResult result = resultActions.andReturn();
		String contentAsString = result.getResponse().getContentAsString();

		final ObjectMapper objectMapper = new ObjectMapper();
		Product[] products = objectMapper.readValue(contentAsString, Product[].class);

		Assertions.assertEquals(2, products.length);
		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/%s/sell", products[0].getId()))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotAcceptable()).andReturn();

		contentAsString = sellResult.getResponse().getContentAsString();
		Assertions
				.assertTrue(contentAsString.contains(String.format("Product %s is out of stock", products[0].getId())));
	}

	@Test
	@DisplayName("Test sell of Product with one article missing from inventory")
	public void testSellProductWithOneArticleMissing_Http_406() throws Exception {
		String contentAsString = null;
		Product[] products;

		// fill product
		{
			MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"products\":[{\"name\":\"VIGRAD\",\"contain_articles\":[{\"art_id\":300,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/product/upload")
					.file(productJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			final ObjectMapper objectMapper = new ObjectMapper();
			products = objectMapper.readValue(contentAsString, Product[].class);

			Assertions.assertEquals(2, products.length);
		}

		// fill articles
		{
			MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25},{\"art_id\":3,\"name\":\"PLASMOS\",\"stock\":17},{\"art_id\":4,\"name\":\"COGNICODE\",\"stock\":15},{\"art_id\":5,\"name\":\"MAXEMIA\",\"stock\":23},{\"art_id\":6,\"name\":\"ECLIPSENT\",\"stock\":28},{\"art_id\":7,\"name\":\"CEDWARD\",\"stock\":29},{\"art_id\":8,\"name\":\"DEEPENDS\",\"stock\":16},{\"art_id\":9,\"name\":\"ELEMANTRA\",\"stock\":23},{\"art_id\":10,\"name\":\"ICOLOGY\",\"stock\":16},{\"art_id\":11,\"name\":\"KATAKANA\",\"stock\":21},{\"art_id\":12,\"name\":\"MUSIX\",\"stock\":29},{\"art_id\":13,\"name\":\"XLEEN\",\"stock\":21},{\"art_id\":14,\"name\":\"OVERPLEX\",\"stock\":29},{\"art_id\":15,\"name\":\"EMTRAC\",\"stock\":26},{\"art_id\":16,\"name\":\"BEDLAM\",\"stock\":27},{\"art_id\":17,\"name\":\"JIMBIES\",\"stock\":23},{\"art_id\":18,\"name\":\"AMRIL\",\"stock\":20},{\"art_id\":19,\"name\":\"WAZZU\",\"stock\":23},{\"art_id\":20,\"name\":\"CIPROMOX\",\"stock\":16},{\"art_id\":21,\"name\":\"COMBOT\",\"stock\":18},{\"art_id\":22,\"name\":\"JOVIOLD\",\"stock\":30},{\"art_id\":23,\"name\":\"QUINTITY\",\"stock\":21},{\"art_id\":24,\"name\":\"ERSUM\",\"stock\":25},{\"art_id\":25,\"name\":\"OPPORTECH\",\"stock\":19},{\"art_id\":26,\"name\":\"OZEAN\",\"stock\":30},{\"art_id\":27,\"name\":\"MARTGO\",\"stock\":24},{\"art_id\":28,\"name\":\"APPLICA\",\"stock\":15},{\"art_id\":29,\"name\":\"UTARIAN\",\"stock\":24},{\"art_id\":30,\"name\":\"TALKOLA\",\"stock\":30},{\"art_id\":31,\"name\":\"ECOLIGHT\",\"stock\":24},{\"art_id\":32,\"name\":\"CINESANCT\",\"stock\":24},{\"art_id\":33,\"name\":\"TRI@TRIBALOG\",\"stock\":21},{\"art_id\":34,\"name\":\"BIOSPAN\",\"stock\":22}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/article/upload")
					.file(articleJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			final ObjectMapper objectMapper = new ObjectMapper();
			Article[] articles = objectMapper.readValue(contentAsString, Article[].class);

			Assertions.assertEquals(35, articles.length);
		}

		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/%s/sell", products[0].getId()))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotAcceptable()).andReturn();

		contentAsString = sellResult.getResponse().getContentAsString();
		Assertions
				.assertTrue(contentAsString.contains(String.format("Product %s is out of stock", products[0].getId())));
	}

	@Test
	@DisplayName("Test sell of Product")
	public void testSellProduct_Http_200() throws Exception {
		String contentAsString = null;
		Product[] products;
		final ObjectMapper objectMapper = new ObjectMapper();

		// fill articles
		{
			MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25},{\"art_id\":3,\"name\":\"PLASMOS\",\"stock\":17},{\"art_id\":4,\"name\":\"COGNICODE\",\"stock\":15},{\"art_id\":5,\"name\":\"MAXEMIA\",\"stock\":23},{\"art_id\":6,\"name\":\"ECLIPSENT\",\"stock\":28},{\"art_id\":7,\"name\":\"CEDWARD\",\"stock\":29},{\"art_id\":8,\"name\":\"DEEPENDS\",\"stock\":16},{\"art_id\":9,\"name\":\"ELEMANTRA\",\"stock\":23},{\"art_id\":10,\"name\":\"ICOLOGY\",\"stock\":16},{\"art_id\":11,\"name\":\"KATAKANA\",\"stock\":21},{\"art_id\":12,\"name\":\"MUSIX\",\"stock\":29},{\"art_id\":13,\"name\":\"XLEEN\",\"stock\":21},{\"art_id\":14,\"name\":\"OVERPLEX\",\"stock\":29},{\"art_id\":15,\"name\":\"EMTRAC\",\"stock\":26},{\"art_id\":16,\"name\":\"BEDLAM\",\"stock\":27},{\"art_id\":17,\"name\":\"JIMBIES\",\"stock\":23},{\"art_id\":18,\"name\":\"AMRIL\",\"stock\":20},{\"art_id\":19,\"name\":\"WAZZU\",\"stock\":23},{\"art_id\":20,\"name\":\"CIPROMOX\",\"stock\":16},{\"art_id\":21,\"name\":\"COMBOT\",\"stock\":18},{\"art_id\":22,\"name\":\"JOVIOLD\",\"stock\":30},{\"art_id\":23,\"name\":\"QUINTITY\",\"stock\":21},{\"art_id\":24,\"name\":\"ERSUM\",\"stock\":25},{\"art_id\":25,\"name\":\"OPPORTECH\",\"stock\":19},{\"art_id\":26,\"name\":\"OZEAN\",\"stock\":30},{\"art_id\":27,\"name\":\"MARTGO\",\"stock\":24},{\"art_id\":28,\"name\":\"APPLICA\",\"stock\":15},{\"art_id\":29,\"name\":\"UTARIAN\",\"stock\":24},{\"art_id\":30,\"name\":\"TALKOLA\",\"stock\":30},{\"art_id\":31,\"name\":\"ECOLIGHT\",\"stock\":24},{\"art_id\":32,\"name\":\"CINESANCT\",\"stock\":24},{\"art_id\":33,\"name\":\"TRI@TRIBALOG\",\"stock\":21},{\"art_id\":34,\"name\":\"BIOSPAN\",\"stock\":22}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/article/upload")
					.file(articleJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			Article[] articles = objectMapper.readValue(contentAsString, Article[].class);

			Assertions.assertEquals(35, articles.length);
		}

		// fill product
		{
			MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"products\":[{\"name\":\"VIGRAD\",\"contain_articles\":[{\"art_id\":18,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/product/upload")
					.file(productJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			products = objectMapper.readValue(contentAsString, Product[].class);

			Assertions.assertEquals(2, products.length);
		}

		// sell the product 1
		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/%s/sell", products[0].getId()))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		contentAsString = sellResult.getResponse().getContentAsString();

		Product soldProduct = objectMapper.readValue(contentAsString, Product.class);

		Assertions.assertEquals(products[0].getStock(), 1);
		Assertions.assertEquals(soldProduct.getStock(), 0);
	}

	@Test
	@DisplayName("Test sell of out of stock Product")
	public void testSellOfOutOfProduct_Http_200() throws Exception {
		String contentAsString = null;
		Product[] products;
		final ObjectMapper objectMapper = new ObjectMapper();

		// fill articles
		{
			MockMultipartFile articleJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"inventory\":[{\"art_id\":0,\"name\":\"UNCORP\",\"stock\":16},{\"art_id\":1,\"name\":\"BLANET\",\"stock\":18},{\"art_id\":2,\"name\":\"DYMI\",\"stock\":25},{\"art_id\":3,\"name\":\"PLASMOS\",\"stock\":17},{\"art_id\":4,\"name\":\"COGNICODE\",\"stock\":15},{\"art_id\":5,\"name\":\"MAXEMIA\",\"stock\":23},{\"art_id\":6,\"name\":\"ECLIPSENT\",\"stock\":28},{\"art_id\":7,\"name\":\"CEDWARD\",\"stock\":29},{\"art_id\":8,\"name\":\"DEEPENDS\",\"stock\":16},{\"art_id\":9,\"name\":\"ELEMANTRA\",\"stock\":23},{\"art_id\":10,\"name\":\"ICOLOGY\",\"stock\":16},{\"art_id\":11,\"name\":\"KATAKANA\",\"stock\":21},{\"art_id\":12,\"name\":\"MUSIX\",\"stock\":29},{\"art_id\":13,\"name\":\"XLEEN\",\"stock\":21},{\"art_id\":14,\"name\":\"OVERPLEX\",\"stock\":29},{\"art_id\":15,\"name\":\"EMTRAC\",\"stock\":26},{\"art_id\":16,\"name\":\"BEDLAM\",\"stock\":27},{\"art_id\":17,\"name\":\"JIMBIES\",\"stock\":23},{\"art_id\":18,\"name\":\"AMRIL\",\"stock\":20},{\"art_id\":19,\"name\":\"WAZZU\",\"stock\":23},{\"art_id\":20,\"name\":\"CIPROMOX\",\"stock\":16},{\"art_id\":21,\"name\":\"COMBOT\",\"stock\":18},{\"art_id\":22,\"name\":\"JOVIOLD\",\"stock\":30},{\"art_id\":23,\"name\":\"QUINTITY\",\"stock\":21},{\"art_id\":24,\"name\":\"ERSUM\",\"stock\":25},{\"art_id\":25,\"name\":\"OPPORTECH\",\"stock\":19},{\"art_id\":26,\"name\":\"OZEAN\",\"stock\":30},{\"art_id\":27,\"name\":\"MARTGO\",\"stock\":24},{\"art_id\":28,\"name\":\"APPLICA\",\"stock\":15},{\"art_id\":29,\"name\":\"UTARIAN\",\"stock\":24},{\"art_id\":30,\"name\":\"TALKOLA\",\"stock\":30},{\"art_id\":31,\"name\":\"ECOLIGHT\",\"stock\":24},{\"art_id\":32,\"name\":\"CINESANCT\",\"stock\":24},{\"art_id\":33,\"name\":\"TRI@TRIBALOG\",\"stock\":21},{\"art_id\":34,\"name\":\"BIOSPAN\",\"stock\":22}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/article/upload")
					.file(articleJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			Article[] articles = objectMapper.readValue(contentAsString, Article[].class);

			Assertions.assertEquals(35, articles.length);
		}

		// fill product
		{
			MockMultipartFile productJsonFile = new MockMultipartFile("file", "filename.json", "text/plain",
					"{\"products\":[{\"name\":\"VIGRAD\",\"contain_articles\":[{\"art_id\":18,\"amount_of\":2},{\"art_id\":24,\"amount_of\":1},{\"art_id\":21,\"amount_of\":8},{\"art_id\":3,\"amount_of\":8},{\"art_id\":23,\"amount_of\":7},{\"art_id\":28,\"amount_of\":8},{\"art_id\":4,\"amount_of\":8}]},{\"name\":\"INTRAAR\",\"contain_articles\":[{\"art_id\":14,\"amount_of\":7},{\"art_id\":1,\"amount_of\":2},{\"art_id\":20,\"amount_of\":5},{\"art_id\":17,\"amount_of\":1},{\"art_id\":25,\"amount_of\":2},{\"art_id\":21,\"amount_of\":7},{\"art_id\":4,\"amount_of\":5}]}]}"
							.getBytes());
			ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.multipart("/product/upload")
					.file(productJsonFile).accept(MediaType.APPLICATION_JSON))
					.andExpect(MockMvcResultMatchers.status().isOk());

			MvcResult result = resultActions.andReturn();
			contentAsString = result.getResponse().getContentAsString();

			products = objectMapper.readValue(contentAsString, Product[].class);

			Assertions.assertEquals(2, products.length);
		}

		// sell the product 1
		MvcResult sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/%s/sell", products[0].getId()))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		contentAsString = sellResult.getResponse().getContentAsString();

		Product soldProduct = objectMapper.readValue(contentAsString, Product.class);

		Assertions.assertEquals(products[0].getStock(), 1);
		Assertions.assertEquals(soldProduct.getStock(), 0);

		// sell again but its out of stock now
		sellResult = mockMvc
				.perform(MockMvcRequestBuilders.post(String.format("/product/%s/sell", products[0].getId()))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotAcceptable()).andReturn();

		contentAsString = sellResult.getResponse().getContentAsString();
		Assertions.assertTrue(contentAsString.contains(
				String.format("Product %s is out of stock due to article 4 being out of stock", products[0].getId())));
	}

}
