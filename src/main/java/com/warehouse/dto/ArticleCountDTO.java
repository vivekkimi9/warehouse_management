package com.warehouse.dto;

import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@Data
public class ArticleCountDTO {
	private Integer art_id;
	private Integer amount_of;
}
