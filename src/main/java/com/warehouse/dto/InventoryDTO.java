package com.warehouse.dto;

import java.util.List;

import com.warehouse.data.entity.Article;

import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@Data
public class InventoryDTO {
	private List<Article> inventory;
}
