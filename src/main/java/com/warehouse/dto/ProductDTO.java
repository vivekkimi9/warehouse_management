package com.warehouse.dto;

import java.util.List;


import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@Data
public class ProductDTO {
	private String id;
	private String name;
	private List<ArticleCountDTO> contain_articles;
}
