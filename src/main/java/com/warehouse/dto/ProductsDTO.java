package com.warehouse.dto;

import java.util.List;

import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@Data
public class ProductsDTO {
	private List<ProductDTO> products;
}
