/**
 * 
 */
package com.warehouse.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.warehouse.core.exceptions.NotFoundException;
import com.warehouse.core.exceptions.OutOfStockException;
import com.warehouse.data.entity.Product;
import com.warehouse.dto.ProductDTO;

/**
 * @author viveksachidananda
 *
 */
public interface IProductService extends IWarehouseService {
	Page<Product> getProducts(Pageable pageable);

	Product save(ProductDTO product);
	
	List<Product> saveAll(List<ProductDTO> products);

	Product sellById(String id) throws SQLException, OutOfStockException, NotFoundException;

	Product sellByName(String name);
}
