package com.warehouse.services;

import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import com.warehouse.core.exceptions.GenericWarehouseException;

public interface IWarehouseService {
	Map<String, Integer> updateProductStock(Set<Integer> articleId) throws GenericWarehouseException, SQLException;
}
