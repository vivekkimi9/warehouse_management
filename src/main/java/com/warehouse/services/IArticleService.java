/**
 * 
 */
package com.warehouse.services;

import java.sql.SQLException;
import java.util.List;

import com.warehouse.core.exceptions.GenericWarehouseException;
import com.warehouse.data.entity.Article;

/**
 * @author viveksachidananda
 *
 */
public interface IArticleService extends IWarehouseService {
	List<Article> saveAll(List<Article> articles) throws GenericWarehouseException, SQLException;

	Article getArticle(Integer articleId);
}
