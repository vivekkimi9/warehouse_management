package com.warehouse.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.warehouse.core.DBManager.DBAccess;
import com.warehouse.core.exceptions.NotFoundException;
import com.warehouse.core.exceptions.OutOfStockException;
import com.warehouse.data.IArticleRepository;
import com.warehouse.data.entity.Article;
import com.warehouse.data.entity.Product;
import com.warehouse.data.entity.ProductArticle;
import com.warehouse.dto.ArticleCountDTO;
import com.warehouse.dto.ProductDTO;
import com.warehouse.services.IProductService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author viveksachidananda
 *
 */
@Slf4j
@Service
public class ProductServiceImpl extends WarehouseServiceImpl implements IProductService {

	@Autowired
	private IArticleRepository articleRepo;

	/**
	 * Given the productId and list of articles associated with the product, this
	 * function saves them to DB
	 * 
	 * @param productId
	 * @param articles
	 * @return List of ProductArticle
	 */
	@Transactional
	private List<ProductArticle> saveProductArticles(final String productId, final List<ArticleCountDTO> articles) {
		List<ProductArticle> productArticleBatch = new ArrayList<>();
		List<ProductArticle> result = new ArrayList<>();

		ProductArticle prodArticle = null;
		for (int i = 0; i < articles.size(); i++) {

			prodArticle = new ProductArticle();
			ArticleCountDTO article = articles.get(i);
			prodArticle.setArticleId(article.getArt_id());
			prodArticle.setAmount(article.getAmount_of());
			prodArticle.setProductId(productId);

			productArticleBatch.add(prodArticle);

			if (i % ARTICLE_BATCH_SIZE == 0 && i > 0) {
				result.addAll(prodArticleRepo.saveAll(productArticleBatch));
				productArticleBatch.clear();
			}
		}

		if (productArticleBatch.size() > 0) {
			result.addAll(prodArticleRepo.saveAll(productArticleBatch));
			productArticleBatch.clear();
		}

		return result;
	}

	/**
	 * Saves all the products by calculating the Stock value
	 */
	@Override
	@Transactional
	public List<Product> saveAll(List<ProductDTO> products) {
		List<Product> result = new ArrayList<>();
		Product product = null;

		for (ProductDTO productPojo : products) {
			product = new Product();
			product.setName(productPojo.getName());

			Integer stock = Integer.MAX_VALUE;

			// Calculate stock for this product
			{
				// get all the articles and its count (gt than 0) for this product
				Map<Integer, Integer> productIdAndAmountMap = productPojo.getContain_articles().stream()
						.filter(pa -> pa.getAmount_of() > 0)
						.collect(Collectors.toMap(ArticleCountDTO::getArt_id, ArticleCountDTO::getAmount_of));

				// fetch corresponding articles from the inventory
				List<Article> articles = articleRepo.findAllByArticleIdIn(productIdAndAmountMap.keySet());

				// calculate the product stock based on current inventory
				for (Article article : articles) {
					Integer articleNeeded = productIdAndAmountMap.get(article.getArticleId());
					int available = articleNeeded != null ? article.getStock() / articleNeeded : 0;
					stock = stock > available ? available : stock;

					// if we have found the article in the inventory then we can remove it
					productIdAndAmountMap.remove(article.getArticleId());
				}

				// all the map elements must be removed else there is an article needed for this
				// product but not in the inventory
				stock = Integer.MAX_VALUE == stock ? 0 : (productIdAndAmountMap.isEmpty() ? stock : 0);
			}

			// set the product stock
			product.setStock(stock);

			// save the product
			product = productRepo.save(product);

			// then save the product articles
			saveProductArticles(product.getId(), productPojo.getContain_articles());

			result.add(product);
		}

		return result;
	}

	@Override
	public Page<Product> getProducts(Pageable pageable) {
		return productRepo.findAll(pageable);
	}

	/**
	 * Sells the product and updates the stock for articles and products post sell
	 * 
	 * The logic is as follows, 1. Lock the PRODUCT row matching the ID, so that no
	 * one else is trying to buy the same product 2. Lock the ARTICLE'S to ensure
	 * sufficient articles are present during the sell 3. Lock PRODUCT_ARTICLE for
	 * this PRODUCT to ensure no is updating the required article amount
	 * 
	 * Once all the above locks are obtained then we can re-calculate the product
	 * stock and update the new stock for this product and all other products
	 * associated to product articles
	 */
	@Override
	public Product sellById(String id) throws SQLException, OutOfStockException, NotFoundException {
		Connection conn = dbManager.getConnection(DBAccess.READWRITE);

		try {
			// set auto commit to false to ensure we manage the transactions
			conn.setAutoCommit(false);
	
			// Lock 1 : PRODUCT table
			String sqlQuery = String.format("SELECT id FROM product WHERE id = '%s' FOR UPDATE", id);
			ResultSet rs = conn.createStatement().executeQuery(sqlQuery);
	
			String productId = null;
	
			while (rs.next()) {
				productId = rs.getString("id");
			}
			
			// throw product not found exception and Unlock 1
			if (productId == null) {
				log.info(String.format("Product %s is not found", id));
				throw new NotFoundException(String.format("Product %s is not found", id));
			}
	
			// Lock 2 : Product Article table
			sqlQuery = String.format(
					"SELECT article_id as articleId, amount FROM product_article WHERE product_id = '%s' FOR UPDATE", id);
			rs = conn.createStatement().executeQuery(sqlQuery);
	
			Map<Integer, Integer> articleAmountMap = new HashMap<>();
			while (rs.next()) {
				articleAmountMap.put(rs.getInt("articleId"), rs.getInt("amount"));
			}
	
			String articleIdsForQuery = articleAmountMap.keySet().toString();
			articleIdsForQuery = articleIdsForQuery.substring(1, articleIdsForQuery.length() - 1);
	
			// Lock 3 : Articles table
			sqlQuery = String.format(
					"SELECT article_id as articleId, stock FROM article WHERE article_id in (%s) FOR UPDATE",
					articleIdsForQuery);
	
			Map<Integer, Integer> articleInventoryCountMap = new HashMap<>();
			rs = conn.createStatement().executeQuery(sqlQuery);
	
			// get the articles and its stock
			while (rs.next()) {
				articleInventoryCountMap.put(rs.getInt("articleId"), rs.getInt("stock"));
			}
			
			// check if the article under stock satisfies the product article requirement
			if (articleInventoryCountMap.size() == articleAmountMap.size()) {
				for (Map.Entry<Integer, Integer> entry : articleAmountMap.entrySet()) {
					Integer articleInventory = articleInventoryCountMap.get(entry.getKey());
	
					if (articleInventory != null && articleInventory >= entry.getValue()) {
						articleInventoryCountMap.put(entry.getKey(), articleInventory - entry.getValue());
					} else {
						// one of the article is not in stock so unlock Lock 1,2,3
						throw new OutOfStockException(String.format(
								"Product %s is out of stock due to article %s being out of stock", id, entry.getKey()));
					}
				}
	
				// update the product stock
				sqlQuery = String.format("UPDATE product SET stock = (stock - 1) WHERE id = '%s'", id);
				conn.createStatement().executeUpdate(sqlQuery);
	
				// update the article stock
				{
					PreparedStatement updateArticleStatement = conn
							.prepareStatement("UPDATE article SET stock = ? WHERE article_id = ?");
	
					for (Map.Entry<Integer, Integer> newInventoryEntry : articleInventoryCountMap.entrySet()) {
						updateArticleStatement.setInt(1, newInventoryEntry.getValue());
						updateArticleStatement.setInt(2, newInventoryEntry.getKey());
						updateArticleStatement.addBatch();
					}
	
					updateArticleStatement.executeBatch();
				}
				
				// finally commit all the transactions and unlock
				conn.commit();
				conn.close();
	
				// now update all other products that are linked to these articles
				try {
					updateProductStock(articleInventoryCountMap.keySet());
				} catch (Exception e) {
					log.error("Product %s sold but failed to update other product stock %s", id, e.getMessage());
					e.printStackTrace();
				}
			} else {
				// There is an article missing from the inventory hence close the connection and
				// unlock Lock 1,2,3
				log.info(String.format("Product %s is out of stock", id));
				throw new OutOfStockException(String.format("Product %s is out of stock", id));
			}
		} catch (Exception e) {
			// close the connection in worst scenario
			if (conn != null) {
				conn.close();
			}
			
			// rethrow as the router handles generic exception and wraps to custom
			throw e;
		}

		// ensure the product is updated and hence get it from the DB
		Optional<Product> optProduct = productRepo.findById(id);

		return optProduct.isEmpty() ? null : optProduct.get();
	}

	@Override
	public Product sellByName(String name) {
		// TODO: Implement it
		return null;
	}

	@Override
	public Product save(ProductDTO product) {
		List<ProductDTO> productsDTO = new ArrayList<>();
		productsDTO.add(product);

		List<Product> products = saveAll(productsDTO);

		return products.size() == 1 ? products.get(0) : null;
	}

}
