/**
 * 
 */
package com.warehouse.services.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.core.exceptions.GenericWarehouseException;
//import com.warehouse.data.ArticleBulkUpload;
import com.warehouse.data.IArticleRepository;
import com.warehouse.data.entity.Article;
import com.warehouse.services.IArticleService;

/**
 * @author viveksachidananda
 *
 */

@Service
@Transactional
public class ArticleServiceImpl extends WarehouseServiceImpl implements IArticleService {

	@Autowired
	private IArticleRepository articleRepo;

	@Override
	public Article getArticle(Integer articleId) {
		var optArticle = articleRepo.findByArticleId(articleId);
		Article article = !optArticle.isEmpty() ? optArticle.get() : null;
		return article;
	}

	/**
	 * Saves the articles
	 */
	@Override
	public List<Article> saveAll(List<Article> articles) throws GenericWarehouseException, SQLException {
		List<Article> result = new ArrayList<>();
		List<Article> articleBatch = new ArrayList<>();

		for (int i = 0; i < articles.size(); i++) {
			Article article = articles.get(i);
			articleBatch.add(article);

			if (i % ARTICLE_BATCH_SIZE == (ARTICLE_BATCH_SIZE - 1)) {
				result.addAll(articleRepo.saveAll(articleBatch));
				articleBatch.clear();
			}
		}

		if (articleBatch.size() > 0) {
			result.addAll(articleRepo.saveAll(articleBatch));
			articleBatch.clear();
		}

		articleRepo.flush();

		return result;
	}

}
