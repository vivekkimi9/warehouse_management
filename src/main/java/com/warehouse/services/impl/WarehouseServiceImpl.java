package com.warehouse.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.warehouse.core.DBManager;
import com.warehouse.core.DBManager.DBAccess;
import com.warehouse.core.exceptions.GenericWarehouseException;
import com.warehouse.data.IProductArticleRepository;
import com.warehouse.data.IProductRepository;
import com.warehouse.data.entity.ProductArticle;
import com.warehouse.services.IWarehouseService;

/**
 * @author viveksachidananda
 *
 */
@Service
public class WarehouseServiceImpl implements IWarehouseService {

	@Value("${warehouse.article.upload.batch-size}")
	protected int ARTICLE_BATCH_SIZE;

	@Autowired
	DBManager dbManager;

	@Autowired
	protected IProductRepository productRepo;

	@Autowired
	protected IProductArticleRepository prodArticleRepo;

	/**
	 * @param articleIds
	 * @param oProductIdStockMap (output)
	 * @throws GenericWarehouseException
	 * @throws SQLException
	 */
	private void executeProductStockUpdate(final List<Integer> articleIds, Map<String, Integer> oProductIdStockMap)
			throws GenericWarehouseException, SQLException {
		// get all the products linked to the articles
		List<ProductArticle> productArticles = prodArticleRepo.findAllByArticleIdIn(articleIds);

		if (!productArticles.isEmpty()) {
			Map<String, Integer> newProductStockMap = new HashMap<>();

			String productIdsForQuery = productArticles.stream().map(pa -> "'" + pa.getProductId() + "'")
					.collect(Collectors.joining(","));
			
			// Get the new stock details for each of the product in the map
			String sqlQuery = String.format(
					"SELECT pa.product_id as productId, MIN(CASE WHEN (a.stock/pa.amount) IS NULL THEN 0 ELSE (a.stock/pa.amount) END) as stock \n"
							+ "        FROM product_article pa LEFT JOIN article a ON a.article_id = pa.article_id WHERE pa.product_id IN (%s) GROUP BY pa.product_id",
							productIdsForQuery);

			Connection conn = dbManager.getConnection(DBAccess.READWRITE);
			
			try {
				Statement queryStatement = conn.createStatement();
				ResultSet rs = queryStatement.executeQuery(sqlQuery);
	
				Integer stock = null;
				String productId = null;
				
				// populate the map
				while (rs.next()) {
					productId = rs.getString("productId");
					stock = rs.getInt("stock");
					newProductStockMap.put(productId, stock);
					oProductIdStockMap.put(productId, stock);
				}
	
				// update the products based on the new stock
				{
					PreparedStatement updateStatement = conn.prepareStatement("UPDATE product SET stock = ? WHERE id = ?");
					conn.setAutoCommit(false);
	
					// we now have the new values of stock for the products, lets update them
					if (!newProductStockMap.isEmpty()) {
						for (Map.Entry<String, Integer> entry : newProductStockMap.entrySet()) {
							updateStatement.setInt(1, entry.getValue());
							updateStatement.setString(2, entry.getKey());
							updateStatement.addBatch();
						}
	
						if (updateStatement.getFetchSize() > 0) {
							updateStatement.executeBatch();
							conn.commit();
						}
					}
				}
			} catch (Exception e) {
				if (conn != null) {
					conn.close();
				}
				
				throw e;
			}
		}
	}

	/**
	 * Updates the product stock related to the articles mentioned in the set
	 */
	/**
	 *
	 */
	@Override
	public Map<String, Integer> updateProductStock(Set<Integer> articleIds)
			throws GenericWarehouseException, SQLException {
		Map<String, Integer> productIdStockMap = new HashMap<>();
		List<Integer> articleBatch = new ArrayList<>();

		int count = 0;

		for (Integer articleId : articleIds) {
			articleBatch.add(articleId);

			if (count % ARTICLE_BATCH_SIZE == (ARTICLE_BATCH_SIZE - 1)) {
				executeProductStockUpdate(articleBatch, productIdStockMap);
				articleBatch.clear();
			}

			count++;
		}

		if (articleBatch.size() > 0) {
			executeProductStockUpdate(articleBatch, productIdStockMap);
			articleBatch.clear();
		}

		return productIdStockMap;
	}

}
