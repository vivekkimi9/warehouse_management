package com.warehouse.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.warehouse.data.entity.Product;

/**
 * @author viveksachidananda Provides JPA for Product table
 */
public interface IProductRepository extends JpaRepository<Product, String> {
}
