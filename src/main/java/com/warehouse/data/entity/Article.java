package com.warehouse.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
@Table(indexes = @Index(columnList = "articleId"))
public class Article {
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy = "uuid")
	@Column(columnDefinition = "BINARY(16)")
	private String id;
	
	@JsonProperty("art_id")
	@NotNull
	@Column(unique = true)
	private Integer articleId;
	
	@NotNull
	private String name;
	
	@NotNull
	private Integer stock;
}
