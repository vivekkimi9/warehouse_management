package com.warehouse.data.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.sun.istack.NotNull;

import lombok.Data;

/**
 * @author viveksachidananda
 *
 */
@Data
@Entity
@Table(uniqueConstraints={ @UniqueConstraint(columnNames = {"productId", "articleId"})})
public class ProductArticle {
	@Id
	@GeneratedValue
	private int id;
	
	@NotNull
	@Column(columnDefinition = "BINARY(16)")
	private String productId;
	
	@NotNull
	private Integer articleId;
	
	@NotNull
	private Integer amount;
}
