package com.warehouse.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author viveksachidananda
 *
 */
@Entity
public class Product {
	@Getter
	@Id 
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy = "uuid")
	@Column(columnDefinition = "BINARY(16)")
	private String id;
	
	@Getter
	@Setter
	@NotNull
	@Column(unique = true)
	private String name;
	
	@Getter
	@Setter
	private Integer stock;
}
