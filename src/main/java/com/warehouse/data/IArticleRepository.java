package com.warehouse.data;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.warehouse.data.entity.Article;

/**
 * @author viveksachidananda Provides JPA for Article table
 */
public interface IArticleRepository extends JpaRepository<Article, String> {
	public List<Article> findAllByArticleIdIn(Set<Integer> articleIds);

	public Optional<Article> findByArticleId(Integer articleId);
}
