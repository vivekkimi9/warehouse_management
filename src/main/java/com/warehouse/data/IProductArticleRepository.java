/**
 * 
 */
package com.warehouse.data;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.warehouse.data.entity.ProductArticle;

/**
 * @author viveksachidananda Provides JPA for ProductArticle repository
 */
public interface IProductArticleRepository extends JpaRepository<ProductArticle, Integer> {
	List<ProductArticle> findByProductId(String productId);

	List<ProductArticle> findAllByArticleIdIn(Set<Integer> articleIds);

	List<ProductArticle> findAllByArticleIdIn(List<Integer> articleIds);
}
