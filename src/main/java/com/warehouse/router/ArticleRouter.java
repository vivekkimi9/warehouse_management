package com.warehouse.router;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.core.exceptions.GenericWarehouseException;
import com.warehouse.data.entity.Article;
import com.warehouse.dto.InventoryDTO;
import com.warehouse.services.IArticleService;

import io.swagger.v3.oas.annotations.Operation;

/**
 * @author viveksachidananda
 *
 */
@RestController
public class ArticleRouter {
	
	@Autowired
	private IArticleService service;

	@Operation(summary = "Get Article")
	@GetMapping(value = "/article/{id}", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> getArticle(@PathVariable Integer id) {
		return new ResponseEntity<>(service.getArticle(id), HttpStatus.OK);
	}

	@Operation(summary = "Upload articles via json file")
	@PostMapping(value = "/article/upload", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> uploadArticles(@RequestParam("file") MultipartFile articleJsonFile) throws JsonParseException, JsonMappingException, IOException, GenericWarehouseException, SQLException {
		List<Article> added = null;

		ObjectMapper mapper = new ObjectMapper();
		InventoryDTO inv = mapper.readValue(articleJsonFile.getInputStream(), InventoryDTO.class);

		// add the articles into inventory
		added = service.saveAll(inv.getInventory());
		
		// update the product stock
		service.updateProductStock(added.stream().map(article -> article.getArticleId()).collect(Collectors.toSet()));

		return new ResponseEntity<>(added, HttpStatus.OK);
	}

}
