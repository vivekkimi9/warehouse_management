package com.warehouse.router;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.core.exceptions.GenericWarehouseException;
import com.warehouse.core.exceptions.NotFoundException;
import com.warehouse.core.exceptions.OutOfStockException;
import com.warehouse.dto.ProductDTO;
import com.warehouse.dto.ProductsDTO;
import com.warehouse.services.IProductService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author viveksachidananda
 *
 */
@Slf4j
@RestController
public class ProductRouter {

	@Value("${warehouse,product.page.max-size}")
	private Integer MAX_PAGE_SIZE;

	@Autowired
	private IProductService prodService;

	/**
	 * @param page
	 * @param size
	 * @return Page<Product>
	 */
	@Operation(summary = "Get Products (paginated)")
	@GetMapping(value = "/product", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> getProducts(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "10") int size) throws GenericWarehouseException {
		if (size > MAX_PAGE_SIZE) {
			log.error("Maximum allowed page size is " + MAX_PAGE_SIZE);
			throw new GenericWarehouseException("Maximum allowed page size is " + MAX_PAGE_SIZE);
		}

		return new ResponseEntity<>(prodService.getProducts(PageRequest.of(page, size)), HttpStatus.OK);
	}

	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 * @throws OutOfStockException
	 * @throws NotFoundException
	 */
	@Operation(summary = "Sell Product")
	@PostMapping(value = "/product/{id}/sell", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> sellProduct(@PathVariable String id)
			throws SQLException, OutOfStockException, NotFoundException {
		return new ResponseEntity<>(prodService.sellById(id), HttpStatus.OK);
	}

	/**
	 * @param articleJsonFile
	 * @return List<Product>
	 */
	@Operation(summary = "Upload products via json file")
	@PostMapping(value = "/product/upload", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> uploadProducts(@RequestParam("file") MultipartFile articleJsonFile)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ProductsDTO products = mapper.readValue(articleJsonFile.getInputStream(), ProductsDTO.class);

		return new ResponseEntity<>(prodService.saveAll(products.getProducts()), HttpStatus.OK);
	}

	/**
	 * @param articleJsonFile
	 * @return Product
	 */
	@Operation(summary = "Add a new product")
	@PostMapping(value = "/product", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> addProduct(@RequestBody ProductDTO product) {
		return new ResponseEntity<>(prodService.save(product), HttpStatus.OK);
	}

}
