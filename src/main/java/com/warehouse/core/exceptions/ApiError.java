package com.warehouse.core.exceptions;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * @author viveksachidananda
 *
 */
public class ApiError implements Serializable {

    private static final long serialVersionUID = 1L;

    private String errorCode;
    private String errorMessage;
    private String developerMessage;
    private String errorDocumentationUrl;
    private ZonedDateTime timestamp;

    /**
     * @param errorCode
     * @param errorMessage
     * @param developerMessage
     * @param errorDocumentationUrl
     * @param timestamp
     */
    public ApiError(String errorCode, String errorMessage, String developerMessage, String errorDocumentationUrl, ZonedDateTime timestamp) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.developerMessage = developerMessage;
        this.errorDocumentationUrl = errorDocumentationUrl;
        this.timestamp = timestamp;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public String getErrorDocumentationUrl() {
        return errorDocumentationUrl;
    }

    public void setErrorDocumentationUrl(String errorDocumentationUrl) {
        this.errorDocumentationUrl = errorDocumentationUrl;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
