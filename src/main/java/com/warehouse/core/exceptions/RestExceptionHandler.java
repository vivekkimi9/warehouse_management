package com.warehouse.core.exceptions;

import java.io.IOException;
import java.sql.SQLException;
import java.time.ZonedDateTime;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * @author viveksachidananda
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler {

	@ResponseBody
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
		ApiError apiError = new ApiError(ErrorConstants.NotFound.NOT_FOUND_001, "The requested resource does not exist",
				ex.getMessage(), ErrorConstants.NotFound.DOCS, ZonedDateTime.now());
		return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
	}

	@ResponseBody
	@ExceptionHandler({ Exception.class, InternalServerException.class, IOException.class, JsonParseException.class,
			JsonMappingException.class, SQLException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected ResponseEntity<Object> handleInternalServerException(Exception ex) {
		ApiError apiError = new ApiError(ErrorConstants.InternalError.INTERNAL_ERROR_001,
				"There was internal server error . Resend the request.", "Please contact the customer support",
				ErrorConstants.InternalError.DOCS, ZonedDateTime.now());
		return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ResponseBody
	@ExceptionHandler(GenericWarehouseException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleGenericWarehouseException(GenericWarehouseException ex, WebRequest request) {
		ApiError apiError = new ApiError(ErrorConstants.GenericError.GENERIC_ERROR_001, "A generic error occurred",
				ex.getMessage(), ErrorConstants.GenericError.DOCS, ZonedDateTime.now());
		return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
	}

	@ResponseBody
	@ExceptionHandler(OutOfStockException.class)
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
	protected ResponseEntity<Object> handleOutOfStockException(OutOfStockException ex, WebRequest request) {
		ApiError apiError = new ApiError(ErrorConstants.OutOfStock.OUT_OF_STOCK_001,
				"An out of stock exception occurred", ex.getMessage(), ErrorConstants.OutOfStock.DOCS,
				ZonedDateTime.now());
		return new ResponseEntity<>(apiError, HttpStatus.NOT_ACCEPTABLE);
	}
}
