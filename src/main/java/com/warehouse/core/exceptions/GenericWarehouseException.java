/**
 * 
 */
package com.warehouse.core.exceptions;

/**
 * @author viveksachidananda
 *
 */
public class GenericWarehouseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public GenericWarehouseException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public GenericWarehouseException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public GenericWarehouseException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public GenericWarehouseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
