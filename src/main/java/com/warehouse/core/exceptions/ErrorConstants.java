package com.warehouse.core.exceptions;


/**
 * @author viveksachidananda
 *
 */
public class ErrorConstants {
	
    public static class BadRequest {
        public static final String BAD_REQUEST_001 = "warehouse-error-bad-request-001";
        public static final String DOCS = "https://dummy.docs.warehouse.com/bad-request";
    }
    
    public static class NotFound {
        public static final String NOT_FOUND_001 = "warehouse-error-not-found-001";
        public static final String DOCS = "https://dummy.docs.warehouse.com/not-found";
    }
    
    public static class InternalError {
        public static final String INTERNAL_ERROR_001 = "warehouse-error-internal-error-001";
        public static final String DOCS = "https://dummy.docs.warehouse.com/internal-error";
    }
    
    public static class GenericError {
        public static final String GENERIC_ERROR_001 = "warehouse-error-generic-error-001";
        public static final String DOCS = "https://dummy.docs.warehouse.com/generic-error";
    }
    
    public static class OutOfStock {
        public static final String OUT_OF_STOCK_001 = "warehouse-error-out-of-stock-error-001";
        public static final String DOCS = "https://dummy.docs.warehouse.com/out-of-stock-error";
    }
    
}