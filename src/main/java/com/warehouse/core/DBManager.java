package com.warehouse.core;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author viveksachidananda
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DBManager {
	final static Logger logger = Logger.getLogger(DBManager.class.getName());
	
	public enum DBAccess { READONLY, READWRITE }; 
	
	private String dbUser;
	private String dbPwd;
	private String _jdbcURL;
	
	/**
	 * @param dbName
	 * @param dbUser
	 * @param dbPwd
	 */
	@Autowired
	public DBManager(@Value("${spring.datasource.username:root}") String dbUser, @Value("${spring.datasource.password:pwd}") String dbPwd, @Value("${spring.datasource.url:url}") String dbUrl) {
		super();
		this.dbUser = dbUser;
		this.dbPwd = dbPwd;
		_jdbcURL = dbUrl;
	}
	
	
	/**
	 * @param type
	 * @return connection
	 * @throws SQLException
	 */
	public Connection getConnection(DBAccess type) throws SQLException {
		Connection conn = DriverManager.getConnection(_jdbcURL, dbUser, dbPwd);
		conn.setReadOnly(type == DBAccess.READONLY);
		
		return conn;
	}

}

