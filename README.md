# Warehouse by Vivek SACHIDANANDA

Warehouse Management is **Spring Boot** based application under **Java version 11** . 
The system design and architecture is implemented as **Monolithic Service Oriented Architecture** using **REST** api. 

For database it currently uses ***H2*** as in-memory **SQL** database.

## Features
 1. Upload articles and products via file upload.
 2. Upload a product.
 3. Get the products and its availability .
 4. Sell a product.
 
## Pre-requisites
1. ***Docker/Maven*** is the build tool. if it's not installed please install it. (For mac with brew: https://formulae.brew.sh/formula/maven)
2. ***Postman*** app for Adding/Selling product (https://www.postman.com/downloads/)

# How to run
## Via Docker (Easiest)
There is a docker file for this application and docker file automatically builds the JAR file using Maven, so just a simple build and run is sufficient.
> Pre-requisite: Docker installed https://docs.docker.com/get-docker/

1. Run following command from project root directory to build the image  ```docker build -f Dockerfile -t warehouse .```  this will create image locally with name warehouse and tag latest.
2. Run the image with this command  `docker run -p 8080:8080 warehouse`

## Via Command line/IDE
> Pre-requisite: 
> > - *Via IDE*: Your favourite IDE such as **Eclipse/IntelliJ with maven** or
> > - *Via Command line*: **JAVA JDK/JRE and Maven**

- **IDE**
    - Import the application in favourite IDE, and build tool is maven. Run  : maven build ( if IDE has plugins installed )
    - Running application : Right click on `WarehouseApplication.java` file, run as java program
    
- **Command line** : 
    - `mvn install` and once build is complete
    - Run the application using command `mvn spring-boot:run`
    
- **REST Request** :
    - Open **postman** and import the `warehouse.postman_collection.json` under postman-test folder  
    - Upload product/article or Sell product or Get Product via postman app

# Application base path
``localhost:8080/api/v1``

# OpenApi doc url
``http://localhost:8080/api/v1/v3/api-docs``

# Swagger ui url 
```http://localhost:8080/api/v1/swagger-ui/index.html?configUrl=/api/v1/v3/api-docs/swagger-config```

# Run tests
Right click on the test folder and run it with **Junit4**
There are **23 test cases** and achieved coverage for Product and Article services is around **95.9pc**.

![UnitTest](https://bitbucket.org/vivekkimi9/warehouse_management/raw/master/data/UnitTest.png)
![UnitTest_coverage](https://bitbucket.org/vivekkimi9/warehouse_management/raw/master/data/UnitTest_Coverage.png)

# Integration tests
Postman collection has been added in `postman-test` directory.

# Error model
When there are API errors due to internal server, exception etc. A generic error model is built and responded as response.  
```
{
    "errorCode": "warehouse-error-not-found-001",
    "errorMessage": "the requested resource does not exist",
    "developerMessage": "Please contact customer support",
    "errorDocumentationUrl": "https://dummy.docs.warehouse.com/not-found",
    "timestamp": "2021-01-27T23:47:58.242548-06:00"
}
```
